﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.ModelsDb
{
    public class ConfigurationDBSection
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public bool IntegratedSecurity { get; set; }

        public ConfigurationDBSection(IConfiguration cfg)
        {
            var bd = cfg.GetSection("UserDataBase");
            this.Server = bd[nameof(Server)];
            this.Database = bd[nameof(Database)];
            this.User = bd[nameof(User)];
            this.Password = bd[nameof(Password)];
            this.IntegratedSecurity = bd[nameof(IntegratedSecurity)] == "True";
        }

        public string ConnectionString()
            => $@"Server={Server};database={Database};{Security()};pooling=true;";

        private string Security()
        => IntegratedSecurity ? "Integrated Security=true" : $"uid={User};pwd={Password}";
    }
}
