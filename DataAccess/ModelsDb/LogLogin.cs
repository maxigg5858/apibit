﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccess.ModelsDb
{
    public partial class LogLogin
    {
        public int IdLogin { get; set; }
        public int? IdUser { get; set; }
        public string Ip { get; set; }
        public DateTime? Date { get; set; }
    }
}
