﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DataAccess.ModelsDb
{
    public partial class User
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
