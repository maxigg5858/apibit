﻿using DataAccess.ModelsDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repository.IRepository
{
    public interface IUserDA
    {
        bool CreateUser(User user);
        bool Login(User user);
    }
}
