﻿using AutoMapper;
using DataAccess.ModelsDb;
using DataAccess.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class UsersDA : IUserDA
    {

        private readonly IMapper autoMapper;
        private readonly CovidBitContext bdtest;

        public UsersDA()
        {

        }
        public UsersDA( IMapper mapper, CovidBitContext db)
        {
           
            this.autoMapper = mapper;
            this.bdtest = db;
        }
        public bool CreateUser(User user)
        {
            bool ret = false;
            using (var db = new CovidBitContext())
            {
                
                db.Users.Add(user);
                db.SaveChanges();
                ret = true;
            }
            return ret;
        }

        public bool Login(User user)
        {
            bool ret = false;
            try
            {
                using (var db = new CovidBitContext())
                {
                    User searchUser = db.Users.Where(u => u.UserName.Equals(user.UserName) && u.Password.Equals(user.Password)).FirstOrDefault();

                    ret = searchUser != null ? true : false;
                }
            }
            catch (Exception)
            {

                return ret;
            }

            return ret;
        }
    }
}
