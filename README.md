# Workshop Diseño de API ASP.NET Core

## Objetivo
- Diseñar e implementar una API REST para resolver los requerimientos expuestos en el enunciado del workshop siguiendo las recomendaciones de expuestas en la presentación "Workshop Diseño de API REST" incluida en este repositorio. 
 
## Prerequisitos
- Visual Studio 2019 instalado con las herramientas para desarrollo web (ASP.NET Core).
- Descargar y compilar en Visual Studio 2019 la solución "Workshop API ASP.NET Core".

## Consideraciones
- El objetivo del workshop es el diseño de la API a nivel recursos y operaciones (endpoints), sin entrar en mayores detalles de implementación (logs, lógica de negocios, capa de datos) y poder visualizar el resultado en la documentación de la API con Swagger. Para ello, se incluye un método en la solución del workshop como ejemplo.    
- Se estima un tiempo de 60 minutos para desarrollar la API.
- Al finalizar se espera que cada participante muestre, via swagger, la API desarrollada (máximo 10 minutos por persona).

## Enunciado
La entidad financiera Contoso desea migrar su plataforma a una arquitectura basada en servicios REST utilizando ASP.NET Core.  
Contoso maneja la información de clientes (datos personales) y diversos productos (seguros, cuentas, prestamos) por cliente. 
Para algunos tipos de producto se realizan operaciones de crédito y debido que deben quedar registradas.

La API debe permitir el alta, baja y modificación de clientes y
maestro de productos, alta, baja y modificación de un producto a un cliente, 
registrar operaciones de débito y crédito a un producto de un cliente, listar y buscar clientes en base a datos personales como nombre y/o cedula, 
listar las operaciones realizadas en un producto de un cliente y listar los clientes asociados a un tipo de producto específico.

En términos de seguridad y dado que es una API de uso interno de Contoso, todas las operaciones deben recibir el id del usuario que realiza la solicitud,
así como un Access Token.


