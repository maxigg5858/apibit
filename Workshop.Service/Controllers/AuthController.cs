﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using APICOVID.Utils;
using AutoMapper;
using DataAccess.ModelsDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using Workshop.Service.Models;


namespace Workshop.Service.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly IConfiguration configuration;
        private readonly DataAccess.Repository.IRepository.IUserDA userDA;
        private readonly IMapper mapper;

        public AuthController(IConfiguration configuration, DataAccess.Repository.IRepository.IUserDA userDA, IMapper mapper)
        {
            this.configuration = configuration;
            this.userDA = userDA;
            this.mapper = mapper;
        }

        [NonAction]
        public TokenInfo CreateToken(UserAuthModel user)
        {
            string secretKey = configuration["AppSettings:key"];
            var key = Encoding.ASCII.GetBytes(secretKey);

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, "TEST"),
                    new Claim(ClaimTypes.Role, "Reader"),
                    new Claim(ClaimTypes.Version, "V1")
                }),
                Expires = DateTime.Now.AddDays(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
            var token = tokenHandler.CreateToken(tokenDescription);
            return new TokenInfo() { Token = tokenHandler.WriteToken(token), ValidFrom = token.ValidFrom, ValidTo = token.ValidTo };
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody] UserAuthModel user)
        {
            TokenInfo token = new TokenInfo();
            User uLogin = new User { UserName = user.UserName, Password = Encrypt.GetSHA256(user.Password) };
            bool AutOk = this.userDA.Login(uLogin);
            if (AutOk)
            {
                token = CreateToken(user);
                if (token != null)
                    return new ObjectResult(token) { StatusCode = 200 };

            }
            return new ObjectResult(token) { StatusCode = 401 };
        }
    }
}
