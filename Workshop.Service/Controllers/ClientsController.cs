﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using Workshop.Service.Models;
using Workshop.Service.Utils;

namespace Workshop.Service.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ClientsController : ControllerBase
    {

        private IDataTest Data;
        private readonly ILogger<ClientsController> logger;
        public ClientsController(IDataTest data)
        {
            this.Data = data;
            this.logger = logger;
        }

        [HttpGet("{document}")]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Get(string document)
        {
            try
            {
                Client searchClient = (from c in this.Data.clients
                                       where c.Document == document
                                       select c).DefaultIfEmpty().FirstOrDefault();
                if (searchClient != null)
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchClient }) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Delete, Get, Ex {0}.", ex.Message));

                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                if (this.Data != null)
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = this.Data.clients }) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Delete, GetAll, Ex {0}.", ex.Message));

                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = this.Data.clients }) { StatusCode = 400 };
        }
        //  [HttpGet("{document}")]
        /* [HttpGet("Search")]
         [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
         public IActionResult Get(string document, string? name)
         {
             try
             {
                 Client searchClient = (from c in this.Data.clients
                                        where c.Document == document
                                        select c).DefaultIfEmpty().FirstOrDefault();
                 if (searchClient != null)
                     return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchClient }) { StatusCode = 200 };
             }
             catch (Exception ex)
             {
                 logger.LogError(String.Format("Exception Delete, Get, Ex {0}.", ex.Message));

                 return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
             }


             return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
         }
        */
        [HttpDelete("{document}")]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Delete(string document)
        {
            try
            {
                Client searchClient = (from c in this.Data.clients
                                       where c.Document == document
                                       select c).DefaultIfEmpty().FirstOrDefault();
                if (searchClient != null)
                {
                    this.Data.clients.Remove(searchClient);
                    return new OkObjectResult(true);
                }

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Delete, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Post([FromBody] Client client)
        {
            try
            {
                if (this.Data != null)
                    this.Data.clients.Add(client);

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = client }) { StatusCode = 202 };
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Put([FromBody] Client client)
        {
            try
            {
                Client searchClient = (from c in this.Data.clients
                                       where c.Document == client.Document
                                       select c).DefaultIfEmpty().FirstOrDefault();
                if (searchClient != null)
                {
                    searchClient.FullName = client.FullName;
                    searchClient.LastName = client.LastName;
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchClient }) { StatusCode = 200 };
                }

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = client }) { StatusCode = 202 };
        }

        [Route("{document}/Product")]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult PutProductClient(string document, [FromBody] ServiceCls service)
        {
            ServiceCls searchService = new ServiceCls();
            try
            {
                Client searchClient = (from c in this.Data.clients
                                       where c.Document == document
                                       select c).DefaultIfEmpty().FirstOrDefault();
                if (searchClient != null)
                {
                    searchService = (from s in searchClient.Services
                                     where s.IdService == service.IdService
                                     select s).DefaultIfEmpty().FirstOrDefault();
                    if (searchService != null)
                    {
                        searchService.NameService = service.NameService;
                    }
                    else
                    {
                        return new ObjectResult(new ResponseGenerics { Success = false, Message = "Service Not Found", Data = null }) { StatusCode = 400 };
                    }


                }
                else
                    return new ObjectResult(new ResponseGenerics { Success = false, Message = "Service Not Found", Data = null }) { StatusCode = 400 };



            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Update", Data = searchService }) { StatusCode = 202 };
        }
    }
}
