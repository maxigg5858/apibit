﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Workshop.Service.Models;
using Workshop.Service.Utils;

namespace Workshop.Service.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OperationsController : ControllerBase
    {
        private IDataTest Data;
        private readonly ILogger<ServicesController> logger;
        public OperationsController(IDataTest data)
        {
            this.Data = data;
            this.logger = logger;
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Get(int id)
        {
            try
            {
                Operation searchOperation = (from o in this.Data.operations
                                            where o.IdOperation == id
                                            select o).DefaultIfEmpty().FirstOrDefault();
                if (searchOperation != null)
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchOperation }) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Operations, Get, Ex {0}.", ex.Message));

                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                if (this.Data.operations != null)
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = this.Data.operations }) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception GetAll, Ex {0}.", ex.Message));

                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = this.Data.operations }) { StatusCode = 400 };
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Delete(int id)
        {
            try
            {
             
                Operation searchOperation = (from o in this.Data.operations
                                             where o.IdOperation == id
                                             select o).DefaultIfEmpty().FirstOrDefault();
                if (searchOperation != null)
                {
                    this.Data.operations.Remove(searchOperation);
                    return new OkObjectResult(true);
                }

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Delete, Operation, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Post([FromBody] Operation operation)
        {
            try
            {
                if (this.Data != null)
                    this.Data.operations.Add(operation);

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, Operation, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = operation }) { StatusCode = 202 };
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Put([FromBody] Operation operation)
        {
            try
            {
                Operation searchOperation = (from o in this.Data.operations
                                             where o.IdOperation == operation.IdOperation
                                             select o).DefaultIfEmpty().FirstOrDefault();
                if (searchOperation != null)
                {

                    searchOperation.CreditDebit = operation.CreditDebit;
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchOperation }) { StatusCode = 200 };
                }

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, Operation, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = operation }) { StatusCode = 202 };
        }
    }
}
