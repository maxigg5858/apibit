﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Workshop.Service.Models;
using Workshop.Service.Utils;

namespace Workshop.Service.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ServicesController : ControllerBase
    {

        private IDataTest Data;
        private readonly ILogger<ServicesController> logger;
        public ServicesController(IDataTest data)
        {
            this.Data = data;
            this.logger = logger;
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Get(int id)
        {
            try
            {
                ServiceCls searchService = (from c in this.Data.services
                                       where c.IdService == id
                                       select c).DefaultIfEmpty().FirstOrDefault();
                if (searchService != null)
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchService }) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Service, Get, Ex {0}.", ex.Message));

                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                if (this.Data != null)
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = this.Data.services }) { StatusCode = 200 };
            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Delete, GetAll, Ex {0}.", ex.Message));

                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = this.Data.services }) { StatusCode = 400 };
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Delete(int id)
        {
            try
            {
                ServiceCls searchService = (from c in this.Data.services
                                            where c.IdService == id
                                            select c).DefaultIfEmpty().FirstOrDefault();
                if (searchService != null)
                {
                    this.Data.services.Remove(searchService);
                    return new OkObjectResult(true);
                }

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Delete, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = false, Message = "Data not found", Data = null }) { StatusCode = 400 };
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Post([FromBody] ServiceCls service)
        {
            try
            {
                if (this.Data != null)
                    this.Data.services.Add(service);

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = service }) { StatusCode = 202 };
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult Put([FromBody] ServiceCls service)
        {
            try
            {
                ServiceCls searchService = (from c in this.Data.services
                                            where c.IdService == service.IdService
                                            select c).DefaultIfEmpty().FirstOrDefault();
                if (searchService != null)
                {
 
                    searchService.NameService = service.NameService;
                    return new ObjectResult(new ResponseGenerics { Success = true, Message = "OK", Data = searchService }) { StatusCode = 200 };
                }

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = service }) { StatusCode = 202 };
        }
    }
}
