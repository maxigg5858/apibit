﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APICOVID.DTO;
using AutoMapper;
using DataAccess.ModelsDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text;
using APICOVID.Utils;

namespace APICOVID.Controllers
{
    [Route("api/[controller]")]
    [ApiController]


    public class UsersController : ControllerBase
    {
        private readonly DataAccess.Repository.IRepository.IUserDA userDA;
        private readonly IMapper mapper;

        public UsersController(DataAccess.Repository.IRepository.IUserDA userDA, IMapper mapper)
        {
            this.userDA = userDA;
            this.mapper = mapper;
        }

        [HttpPost("CreateUser")]
        public IActionResult CreateUser(UserDTO newUser)
        {
            User nUser = new User();
            try
            {
                newUser.Password = Encrypt.GetSHA256(newUser.Password);

                nUser = this.mapper.Map<User>(newUser);
                this.userDA.CreateUser(nUser);
            }
            catch (Exception ex)
            {

                return BadRequest("Error al crear Usuario.");
            }

            return new ObjectResult(nUser) { StatusCode = 201 };
        }

    }
}