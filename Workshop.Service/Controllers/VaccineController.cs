﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Workshop.Service.Models;

namespace APICOVID.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VaccineController : ControllerBase
    {
        private readonly ILogger<VaccineController> logger;
        public VaccineController()
        {
            this.logger = logger;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)] /// RESPUESTA PREDETERMINADA ESPERADA
        public IActionResult GetAll()
        {
            try
            {
               

            }
            catch (Exception ex)
            {
                logger.LogError(String.Format("Exception Post, ClientController, Ex {0}.", ex.Message));
                return new ObjectResult(new ResponseGenerics { Success = false, Message = "Error", Data = null }) { StatusCode = 500 };
            }


            return new ObjectResult(new ResponseGenerics { Success = true, Message = "Created", Data = "" }) { StatusCode = 200 };
        }
    }
}