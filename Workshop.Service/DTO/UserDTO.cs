﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APICOVID.DTO
{
    public class UserDTO
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
