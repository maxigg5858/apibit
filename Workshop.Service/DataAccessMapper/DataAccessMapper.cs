﻿using APICOVID.DTO;
using AutoMapper;
using DataAccess.ModelsDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APICOVID.DataAccessMapper
{
    public class DataAccessMapper:Profile
    {
        public DataAccessMapper()
        {
            CreateMap<User, UserDTO>().ReverseMap(); //Mapeo de modelo BD con DTO
        }
    }
}
