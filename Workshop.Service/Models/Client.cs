﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Workshop.Service.Models
{
    public class Client
    {

        public string FullName { get; set; }
        public string LastName { get; set; }
        public string Document { get; set; }
        public List<ServiceCls> Services { get; set; }

        public Client()
        {
            this.Services = new List<ServiceCls>();
        }

    }
}
