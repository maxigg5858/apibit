﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Workshop.Service.Models
{
    public class Operation
    {
        public int IdOperation { get; set; }
        public DateTime OperationDate { get; set; }
        public string ClientDocument { get; set; }
        public int ProductId { get; set; }
        public string CreditDebit { get; set; }
        public string Description { get; set; }
        public bool Canceled { get; set; }
    }
}
