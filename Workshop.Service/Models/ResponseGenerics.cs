﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Workshop.Service.Models
{
    public class ResponseGenerics
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Object Data{ get; set; }
    }
}
