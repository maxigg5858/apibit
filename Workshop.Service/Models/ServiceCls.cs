﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Workshop.Service.Models
{
    public class ServiceCls
    {
        public int IdService { get; set; }
        public string NameService { get; set; }
    }
}
