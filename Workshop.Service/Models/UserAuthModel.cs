﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Workshop.Service.Models
{
    public class UserAuthModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
