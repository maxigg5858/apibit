﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Workshop.Service.Models;

namespace Workshop.Service.Utils
{
    public class DataTest : IDataTest
    {
        public List<Client> clients { get; set; }
        public List<ServiceCls> services { get; set; }
        public List<Operation> operations { get; set; }
        public DataTest()
        {
            clients = new List<Client>();
            services = new List<ServiceCls>();
            operations = new List<Operation>();
            services.Add(new ServiceCls { IdService = 1, NameService = "Seguro" });
            services.Add(new ServiceCls { IdService = 2, NameService = "Tarjeta" });
            services.Add(new ServiceCls { IdService = 3, NameService = "Telepeaje" });

            Random rnd = new Random();
            int servicioIndex = 0;
            string[] names = new string[12] { "Maxi", "German", "Vero", "Laura", "Roy", "Lujan", "Marcos", "Renzo", "Darwin", "Ruben", "Fernando", "Nico" };
            for (int i = 0; i <= 10; i++)
            {
                servicioIndex = rnd.Next(0, services.Count);

                Client c = new Client()
                {
                    FullName = names[i],
                    LastName = "Workshop",
                    Document = (1234 + i).ToString()
                };


                c.Services.Add(services[servicioIndex]);

                clients.Add(c);
            }

            #region Operations
            Random rnd2 = new Random();
            bool cancelado = false;
            int indexCliente = 0;
            string op = String.Empty;

            for (int i = 0; i <= 10; i++)
            {
                cancelado = i % 2 == 0 ? true : false;
                op = i % 2 == 0 ? "C" : "D";
                indexCliente = rnd2.Next(0, clients.Count);
                servicioIndex = rnd.Next(0, services.Count);
                operations.Add(new Operation
                {
                    ProductId = servicioIndex,
                    Canceled = cancelado,
                    ClientDocument = clients[indexCliente].Document,
                    IdOperation = 1545 + i,
                    Description = String.Format("Test {0}, Index {1}", clients[indexCliente].FullName, i),
                    OperationDate = DateTime.Now,
                    CreditDebit = op
                });
            }

            #endregion
        }
    }
}
