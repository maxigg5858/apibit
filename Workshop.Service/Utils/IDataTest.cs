﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Workshop.Service.Models;

namespace Workshop.Service.Utils
{
    public interface IDataTest
    {
        List<Client> clients { get; set; }
        List<ServiceCls> services { get; set; }
        List<Operation> operations { get; set; }
    }
}
